
import pandas as pd
import re
import numpy as np

from sklearn.svm import SVC

from sklearn.cross_validation import cross_val_score


def get_title(name):
    if pd.isnull(name):
        return "Null"

    title_search = re.search(' ([A-Za-z]+)\.', name)
    if title_search:
        return title_search.group(1).lower()
    else:
        return "None"


def addCols(data_frame):
    # добавил поля Family, isFamily, isCabin, Tittle для дальнейшего анализа
    data_frame['Title'] = pd.Index(data_frame['Name'].apply(get_title))
    data_frame['Family'] = data_frame['Parch'] + data_frame['SibSp']
    data_frame['isFamily'] = data_frame['Family'].apply(lambda r: 1 if r != 0 else 0)
    data_frame['isCabin'] = data_frame['Cabin'].apply(lambda r: 0 if pd.isnull(r) else 1)


def mean_Age(data_frame):
    # для определения среднего возраста использовал поле Title
    # заполнял недостающую информацию средними значениями для тутулов
    ages = data_frame.groupby("Title")["Age"].mean()
    data_frame['Age'] = data_frame.apply(lambda r: ages[r["Title"]] if pd.isnull(r["Age"]) else r["Age"], axis=1)
    return data_frame['Age'].mean()


def format_title(data_frame):
    # replace(to_replace=None, value=None, inplace=False, limit=None, regex=False, method='pad', axis=None)
    data_frame.Title.replace(to_replace=["capt", "col", "dona", "dr",
                        "jonkheer", "lady", "major",
                        "rev", "sir", "countess", "don"], value="aristocratic", inplace=True)
    data_frame.Title.replace("ms", "mrs", inplace=True)
    data_frame.Title.replace(["mlle", "mme"], "miss", inplace=True)


train_location = r'trainstuff\train.csv'
test_location = r'trainstuff\test.csv'

train_df = pd.read_csv(train_location)
test_df = pd.read_csv(test_location)

addCols(train_df)
addCols(test_df)


all_data = pd.concat([train_df, test_df])

format_title(all_data)
format_title(train_df)
format_title(test_df)

mean_ages = all_data.groupby("Title")["Age"].mean()

# заменил все неизвестные значения возраста на средние для титула

all_data['Age'] = all_data.apply(lambda r: mean_ages[r['Title']] if pd.isnull(r["Age"]) else r['Age'], axis=1)
train_df['Age'] = train_df.apply(lambda r: mean_ages[r['Title']] if pd.isnull(r["Age"]) else r['Age'], axis=1)
test_df['Age'] = test_df.apply(lambda r: mean_ages[r['Title']] if pd.isnull(r["Age"]) else r['Age'], axis=1)


#  для лучшей классификации желательно поставить так называемые "заглушки"
#  тоесть поля которые говрят о принадлежности или наличии признака - 1
#  либо о не принадлежности или отстутствии - 0


train_gender_dummies = pd.get_dummies(train_df["Sex"], prefix="SexD", dummy_na=False)
test_gender_dummies = pd.get_dummies(test_df["Sex"], prefix="SexD", dummy_na=False)

#  таким образом на основе существующих данных числовых и не чиловых добовляю соответствующие поля

train_df = pd.concat([train_df, train_gender_dummies], axis=1)
test_df = pd.concat([test_df, test_gender_dummies], axis=1)

# тоже самое с портом отправления

train_embarked_dummies = pd.get_dummies(train_df["Embarked"], prefix="EmbarkedD", dummy_na=False)
test_embarked_dummies = pd.get_dummies(test_df["Embarked"], prefix="EmbarkedD", dummy_na=False)

train_df = pd.concat([train_df, train_embarked_dummies], axis=1)
test_df = pd.concat([test_df, test_embarked_dummies], axis=1)

# титул

train_title_dummies = pd.get_dummies(train_df["Title"], prefix="TitleD", dummy_na=False)
test_title_dummies = pd.get_dummies(test_df["Title"], prefix="TitleD", dummy_na=False)

train_df = pd.concat([train_df, train_title_dummies], axis=1)
test_df = pd.concat([test_df, test_title_dummies], axis=1)


# тк метод опорных вектров предполагает работу с цифрами заменяю строковые признаки числовыми.

# пол

genders = {"male": 1, "female": 0}
train_df["Sex"] = train_df["Sex"].apply(lambda s: genders.get(s))
test_df["Sex"] = test_df["Sex"].apply(lambda s: genders.get(s))

# порт

embarkeds = {"U": 0, "S": 1, "C": 2, "Q": 3}
train_df["Embarked"] = train_df["Embarked"].fillna("U").apply(lambda e: embarkeds.get(e))
test_df["Embarked"] = test_df["Embarked"].fillna("U").apply(lambda e: embarkeds.get(e))

# палуба

decks = {"U": 0, "A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "T": 8}
train_df["DeckF"] = train_df["Cabin"].fillna("U").apply(lambda c: decks.get(c[0], -1))
test_df["DeckF"] = test_df["Cabin"].fillna("U").apply(lambda c: decks.get(c[0], -1))

# титул

titles = train_df['Title'].unique()
titles_test = test_df['Title'].unique()


def get_index(item, array):
    try:
        return np.where(array==item)[0][0]
    except KeyError:
        return -1


train_df["Title"] = train_df["Title"].apply(lambda n: get_index(n, titles))
test_df["Title"] = test_df["Title"].apply(lambda n: get_index(n, titles))
test_df["Fare"] = test_df["Fare"].fillna(0)

# print(train_df.info())
# print(test_df.info())


all_data = pd.concat([train_df, test_df])

# выбрасываю все не числовое

data = train_df.drop(['PassengerId', 'Name', 'Ticket', 'Cabin'], axis=1)
test = test_df.drop(['PassengerId', 'Name', 'Ticket', 'Cabin'], axis=1)

model_svc = SVC()

target = data.Survived
train = data.drop(['Survived'], axis=1)
num_set = 5

# проверка алгоритма на тренировочных данных с использованием кроссвалидации

scores = cross_val_score(model_svc, train, target, cv=num_set)
print(scores.mean())

# тренировка модели и формирование итогового файла

result = pd.DataFrame(test_df.PassengerId)

model_svc.fit(train, target)
result.insert(1, 'Survived', model_svc.predict(test))
result.to_csv('final_test.csv', index=False)

